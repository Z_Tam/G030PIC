#include "lcd.h"
#include "lcd_cfg.h"

uint16_t lcdbuf[16*128] __attribute__((at(0x20001000)));

/********************************************************************
* 函数说明：在指定区域填充颜色
*********************************************************************/
void LCD_Fill(uint16_t xsta,		/* 起始坐标 */
			  uint16_t ysta,
              uint16_t xend,		/* 终止坐标 */
			  uint16_t yend,
			  uint16_t color)		/* 要填充的颜色 */
{          
	uint16_t i,j; 
	LCD_Address_Set(xsta,ysta,xend-1,yend-1);//设置显示范围

	for(i=ysta;i<=yend;i++)
	{													   	 	
		for(j=xsta;j<=xend;j++)
		{
			LCD_WR_DATA16(color);
		}
	} 			
}

void LCD_Fill_Pic(	uint16_t xsta,		/* 起始坐标 */
					uint16_t ysta,
					uint16_t xend,		/* 终止坐标 */
					uint16_t yend,
					uint16_t *pic)		/* 要填充的颜色 */
{
	uint16_t i,j,n=0;
	LCD_Address_Set(xsta,ysta,xend-1,yend-1);//设置显示范围										   	 	
	LCD_CS(GPIO_PIN_RESET);
	LCD_Transmit((uint8_t *)lcdbuf, (xend-xsta)*(yend-ysta)*2);
	LCD_CS(GPIO_PIN_SET);	
}

