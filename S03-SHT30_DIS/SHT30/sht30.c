#include "sht30.h"
#include "i2c.h"
#include "stm32g0xx_hal_conf.h"
#include "stdio.h"

#ifdef HAL_I2C_MODULE_ENABLED
#include "stm32g0xx_hal_i2c.h"
#else
#include "stm32g0xx_ll_i2c.h"
#endif

#ifndef HAL_I2C_MODULE_ENABLED

uint8_t I2C_Write(I2C_TypeDef *I2Cx, unsigned char slave_addr, unsigned char* reg_data, unsigned char data_size,const uint32_t timeout)
{
    volatile uint32_t tickstart = 0;
    unsigned char tx_num = 0;    

    while(LL_I2C_IsActiveFlag_BUSY(I2Cx) == SET)                        /* busy等待 */
    {
        if(tickstart++ >= timeout)
            return 1;
    }
    LL_I2C_HandleTransfer(I2Cx, slave_addr, LL_I2C_ADDRSLAVE_7BIT, data_size, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);  /* 发送地址 */

		tickstart = 0;
    while(tx_num < data_size)
    {
        if(LL_I2C_IsActiveFlag_TXE(I2Cx) && tx_num<data_size)           /* 发送空闲 */
        {
            while(LL_I2C_IsActiveFlag_TXIS(I2Cx) == RESET);             /* 中断状态 */
            LL_I2C_TransmitData8(I2Cx, reg_data[tx_num++]);
						tickstart = 0;
        }
				if(tickstart++ >= timeout)
            return 1;
    }

    tickstart = 0;
    while(LL_I2C_IsActiveFlag_STOP(I2Cx) == RESET)                      /* 等待停止位 */
    {
        if(tickstart++ >= timeout)
            return 1;
    }
    return 0;
}


uint8_t I2C_Read(I2C_TypeDef *I2Cx, uint8_t addr, void *data, uint8_t num,const uint32_t timeout)
{
	uint8_t *rxbuf = data;
	uint8_t count = 0;
  volatile uint32_t tickstart = 0;
	
  while(LL_I2C_IsActiveFlag_BUSY(I2Cx) == SET)                        /* busy等待 */
  {
      if(tickstart++>=timeout)
          return 1;
  }
	
	tickstart = 0;
	LL_I2C_HandleTransfer(I2Cx, addr, LL_I2C_ADDRSLAVE_7BIT, num, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_READ);
	
	while(count < num)
	{
			if(LL_I2C_IsActiveFlag_RXNE(I2Cx))
			{
				rxbuf[count++] = LL_I2C_ReceiveData8(I2Cx);
				tickstart = 0;
			}
			
			if(tickstart++>=timeout)
          return 1;
	}
  LL_I2C_ClearFlag_STOP(I2Cx);
	

	return 0;
}

#endif

/**
 * @brief	向SHT30发送一条指令(16bit)
 * @param	cmd —— SHT30指令（在SHT30_MODE中枚举定义）
 * @retval	成功返回HAL_OK
*/
static uint8_t SHT30_Send_Cmd(SHT30_CMD cmd)
{
    uint8_t cmd_buffer[2];
    cmd_buffer[0] = cmd >> 8;
    cmd_buffer[1] = cmd;
	#ifdef HAL_I2C_MODULE_ENABLED
    return HAL_I2C_Master_Transmit(&hi2c1, SHT30_ADDR_WRITE, (uint8_t*) cmd_buffer, 2, 0xFFFF);
	#else
		return I2C_Write(I2C1, SHT30_ADDR_WRITE,cmd_buffer,2,0xFFFF);
	#endif
}

/**
 * @brief	复位SHT30
 * @param	none
 * @retval	none
*/
void SHT30_reset(void)
{
    SHT30_Send_Cmd(SOFT_RESET_CMD);
    HAL_Delay(20);
}


/**
 * @brief	从SHT30读取一次数据
 * @param	dat —— 存储读取数据的地址（6个字节数组）
 * @retval	成功 —— 返回HAL_OK
*/
uint8_t SHT30_Read_Dat(uint8_t* dat)
{
	SHT30_Send_Cmd(READOUT_FOR_PERIODIC_MODE);
	#ifdef HAL_I2C_MODULE_ENABLED
	return HAL_I2C_Master_Receive(&hi2c1, SHT30_ADDR_READ, dat, 6, 0xFFFF);
	#else
	return I2C_Read(I2C1, SHT30_ADDR_READ, dat, 6,0xFFFF);
	#endif
}

uint8_t CheckCrc8(uint8_t* const message, uint8_t initial_value)
{
    uint8_t  remainder;	    //余数
    uint8_t  i = 0, j = 0;  //循环变量

    /* 初始化 */
    remainder = initial_value;

    for(j = 0; j < 2;j++)
    {
        remainder ^= message[j];

        /* 从最高位开始依次计算  */
        for (i = 0; i < 8; i++)
        {
            if (remainder & 0x80)
            {
                remainder = (remainder << 1)^CRC8_POLYNOMIAL;
            }
            else
            {
                remainder = (remainder << 1);
            }
        }
    }

    /* 返回计算的CRC码 */
    return remainder;
}

/**
 * @brief	将SHT30接收的6个字节数据进行CRC校验，并转换为温度值和湿度值
 * @param	dat  —— 存储接收数据的地址（6个字节数组）
 * @retval	校验成功  —— 返回0
 * 			校验失败  —— 返回1，并设置温度值和湿度值为0
*/
uint8_t SHT30_Dat_To_Float(uint8_t* const dat, float* temperature, float* humidity)
{
	uint16_t recv_temperature = 0;
	uint16_t recv_humidity = 0;
	
	/* 校验温度数据和湿度数据是否接收正确 */
	if(CheckCrc8(dat, 0xFF) != dat[2] || CheckCrc8(&dat[3], 0xFF) != dat[5])
		return 1;
	
	/* 转换温度数据 */
	recv_temperature = ((uint16_t)dat[0]<<8)|dat[1];
	*temperature = -45 + 175*((float)recv_temperature/65535);
	
	/* 转换湿度数据 */
	recv_humidity = ((uint16_t)dat[3]<<8)|dat[4];
	*humidity = 100 * ((float)recv_humidity / 65535);
	
	return 0;
}

/**
 * @brief	初始化SHT30
 * @param	none
 * @retval	成功返回HAL_OK
 * @note	周期测量模式
*/
uint8_t SHT30_Init(void)
{
  SHT30_reset();
  if(SHT30_Send_Cmd(MEDIUM_2_CMD) == HAL_OK)
  {
    printf("sht30 init ok.\n");
    return HAL_OK;
  }
  else 
  {
    printf("sht30 init fail.\n");
    return HAL_ERROR;
  }
}

/**
 * @brief	获取温湿度值
 * @param	none
 * @retval成功返回HAL_OK
 * @note	周期测量模式
*/
uint8_t Sht30_Read_Once(float* temperature, float* humidity)
{
  uint8_t recv_dat[6] = {0};
  if(SHT30_Read_Dat(recv_dat) == HAL_OK)
	{
		if(SHT30_Dat_To_Float(recv_dat, temperature, humidity)==0)
		{
			//printf("temperature = %f, humidity = %f\r\n", *temperature, *humidity);
      return HAL_OK;
		}
		else
		{
			printf("crc check fail.\n");
      return HAL_ERROR;
		}
	}
	else
	{
		//printf("read data from sht30 fail.\n");
    return HAL_ERROR;
	}
}

