#include "Ymodem.h"
#include "main.h"
#include "stdio.h"
#include "string.h"
#include "usart.h"
#include "Flash.h"

enum UPDATE_STATE Update_State = TO_START;	//

/**
 * @bieaf CRC-16 校验
 *
 * @param addr 开始地址
 * @param num   长度
 * @param num   CRC
 * @return crc  返回CRC的值
 */
#define POLY        0x1021  
uint16_t crc16(unsigned char *addr, int num, uint16_t crc)  
{  
    int i;  
    for (; num > 0; num--)					/* Step through bytes in memory */  
    {  
        crc = crc ^ (*addr++ << 8);			/* Fetch byte from memory, XOR into CRC top byte*/  
        for (i = 0; i < 8; i++)				/* Prepare to rotate 8 bits */  
        {
            if (crc & 0x8000)				/* b15 is set... */  
                crc = (crc << 1) ^ POLY;  	/* rotate and XOR with polynomic */  
            else                          	/* b15 is clear... */  
                crc <<= 1;					/* just rotate */  
        }									/* Loop for 8 bits */  
        crc &= 0xFFFF;						/* Ensure CRC remains 16-bit value */  
    }										/* Loop until num=0 */  
    return(crc);							/* Return updated CRC */  
}


/**
 * @bieaf 获取数据包的类型, 顺便进行校验
 *
 * @param buf 开始地址
 * @param len 长度
 * @return 
 */
unsigned char Check_CRC(unsigned char* buf, int len)
{
	unsigned short crc = 0;
	
	/* 进行CRC校验 */
	printf("crc: %x,%x\r\n",buf[0],len);
	if((buf[0]==0x00)&&(len >= 133))
	{
		crc = crc16(buf+3, 128, crc);
		if(crc != (buf[131]<<8|buf[132]))
		{
			return 0;///< 没通过校验
		}
		
		/* 通过校验 */
		return 1;
	}
}


/* 发送指令 */
void send_command(unsigned char command)
{
	HAL_UART_Transmit(&huart1, (uint8_t *)&command,1, 0xFFFF);
	HAL_Delay(10);
}

void ymodem_fun(void)
{
		static unsigned char data_state = TO_START;
		uint64_t update_buf = 0,iupdate_buf = 0;	//16
		static uint32_t addr = ApplicationAddress;
	
		if(data_state == TO_START)
		{
				send_command(CCC);
				HAL_Delay(1000);
		}
		
		if(Rx_Flag == 1)	//接收成功
		{
				Rx_Flag = 0;
				switch(Rx_Buf[0])
				{
					case SOH:
						if(Check_CRC(Rx_Buf, Rx_Len)==1)///< 通过CRC16校验
						{
								//区分不同的数据
								if(Update_State == TO_START 
									&& Rx_Buf[1] == 0x00 && Rx_Buf[2] == 0xFF)	//起始帧
								{
										printf("> Receive start...\r\n");
										Update_State = TO_RECEIVE_DATA;
										data_state = 0x01;
										
										printf("> Receive 1\r\n");
										Erase_page(GetPage(ApplicationAddress),20);	//20页*2k = 40k
										printf("> Erase_page\r\n");
										send_command(ACK);
										send_command(CCC);
										printf("> ACK_success\r\n");
										
								}
								else if(Update_State == TO_RECEIVE_DATA 
									&& Rx_Buf[1] == data_state && Rx_Buf[2] == (unsigned char)(~Rx_Buf[1]))
								{
										printf("> Receive data bag:%d byte\r\n",data_state * 128);
										
										for(int i=3,j=1;i<Rx_Len-2;i++,j++)
										{	
												switch(j%8)
												{
													case 1:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<0*8;
														printf("1:%x \t",Rx_Buf[i]);
													break;
													case 2:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<1*8;
														printf("2:%x \t",Rx_Buf[i]);
													break;
													case 3:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<2*8;
														printf("3:%x \t",Rx_Buf[i]);
													break;
													case 4:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<3*8;
														printf("4:%x \t",Rx_Buf[i]);
													break;
													case 5:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<4*8;
														printf("5:%x \t",Rx_Buf[i]);
													break;
													case 6:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<5*8;
														printf("6:%x \t",Rx_Buf[i]);
													break;
													case 7:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<6*8;
														printf("7:%x \t",Rx_Buf[i]);
													break;
													case 0:
														iupdate_buf = Rx_Buf[i];
														update_buf += iupdate_buf<<7*8;
														HAL_FLASH_Unlock();
														HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, addr, update_buf);	
														HAL_FLASH_Lock();
														addr+=8;
														update_buf = 0;
														printf("0:%x \t\r\n",Rx_Buf[i]);
													break;
													
													default:
														printf("d:%x \t",Rx_Buf[i]);
													break;
												}
											
										}
									
										
										printf("Receive Success\r\n");
										data_state++;						
										send_command(ACK);		
									
								}
								else if(Update_State == TO_RECEIVE_END 
									&& Rx_Buf[1] == 0x00 && Rx_Buf[2] == 0xFF)
								{
										printf("> Receive end...\r\n");
										Update_State = TO_START;
										send_command(ACK);
										
										JumpToApplication();
								}
						}
						else
						{
								printf("> Notpass crc\r\n");
						}
					break;
						
					case EOT:
						if(Update_State == TO_RECEIVE_DATA)
						{
								printf("> Receive EOT1...\r\n");
								Update_State = TO_RECEIVE_EOT2;
								send_command(NACK);
						}
						else if(Update_State == TO_RECEIVE_EOT2)
						{
								printf("> Receive EOT2...\r\n");
								Update_State = TO_RECEIVE_END;
								send_command(ACK);
								send_command(CCC);
						}
						else
						{
								printf("> Receive EOT, But error...\r\n");
						}
						
					break;
					
				}
				
		}

}




