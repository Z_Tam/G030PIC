#include "Flash.h"
#include "stdio.h"

/**
 * @bieaf 擦除页
 *
 * @param pageaddr  起始页	
 * @param num       擦除的页数
 * @return 1
 */
int Erase_page(uint32_t pageaddr, uint32_t num)
{
	HAL_FLASH_Unlock();
	
	/* 擦除FLASH*/
	FLASH_EraseInitTypeDef FlashSet;
	FlashSet.TypeErase = FLASH_TYPEERASE_PAGES;
	FlashSet.Page = pageaddr;
	FlashSet.NbPages = num;
	
	/*设置PageError，调用擦除函数*/
	uint32_t PageError = 0;
	HAL_FLASHEx_Erase(&FlashSet, &PageError);
	
	HAL_FLASH_Lock();
	return 1;
}


/**
 * @bieaf 写若干个DWord数据
 *
 * @param addr       写入的起始地址
 * @param buff       写入数据的数组指针
 * @param dword_size  长度
 */
void WriteFlash(uint32_t addr, uint8_t * buff)
{	


		
}


/**
 * @bieaf 读取指定地址的半字(16位数据)
 *
 * @param faddr  读取地址	
 * @return 16位数据
 */
uint16_t STMFLASH_ReadHalfWord(uint32_t faddr)
{
    return *(volatile uint16_t*)faddr;
}


/**
 * @bieaf 从指定地址开始读出若干个数据（半字）
 *
 * @param ReadAddr: 起始地址
 * @param pBuffer: 数据存储指针（数组）
 * @param NumToRead：读取数据的个数
 * @return 
 */
void STMFLASH_Read(uint32_t ReadAddr,uint16_t *pBuffer,uint16_t NumToRead)     
{
    uint16_t i;
    for(i=0;i<NumToRead;i++)
    {
        pBuffer[i]=STMFLASH_ReadHalfWord(ReadAddr);//读取2个字节.
        ReadAddr+=2;//偏移2个字节.   
    }
}


/**
 * @bieaf 获取传入地址所在的页（一页2k）
 *
 * @param Addr  地址	
 * @return 该地址所在的页
 */
uint32_t GetPage(uint32_t Addr)
{
  uint32_t page = 0;
 
  if (Addr < (FLASH_BASE + FLASH_BANK_SIZE))
  {
    /* Bank 1 */
    page = (Addr - FLASH_BASE) / FLASH_PAGE_SIZE;
  }
  else
  {
    /* Bank 2 */
    page = (Addr - (FLASH_BASE + FLASH_BANK_SIZE)) / FLASH_PAGE_SIZE;
  }
 
  return page;
}


/**
 * @bieaf Flash读写示例
 *
 * @param 
 * @return 
 */
//void FlashTest(void)
//{
//		uint64_t update_flag = 0xABCDF10F12345678;
//		uint16_t buf[10] = {0};
//		
//		Erase_page(GetPage(ApplicationAddress-8), 1);			//1、先擦除页
//		WriteFlash(ApplicationAddress-8,&update_flag,1);	//2、写数据
//		STMFLASH_Read(ApplicationAddress-8,buf,4);				//3、读数据
//		for(int i=0;i<4;i++)
//		{
//				printf("%x",buf[i]);
//		}
//		printf("\r\n");
//		
//}



