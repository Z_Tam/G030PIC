#include "BootLoader.h"
#include "stdio.h"
#include "usart.h"

/*
	显示信息
*/
void show_msg(void)
{
	printf("\r\n");
	printf("***********************************\r\n");
	printf("*                                 *\r\n");
	printf("*            格子科技             *\r\n");
	printf("*                                 *\r\n");
	printf("***********************************\r\n");
	printf("\r\n");
}

/*
	跳转到指定位置
	从BootLoader跳转到App
*/
void JumpToApplication(void)
{
	printf("Bootloader Jump to Application!\r\n");
	if (((*(__IO uint32_t*)ApplicationAddress) & 0x2FFE0000 ) == 0x20000000)	//检查栈顶空间是否合法
	{
			uint32_t JumpAddress = *(__IO uint32_t*) (ApplicationAddress + 4);	
			Jump_Fun Jump_To_Application = (Jump_Fun)JumpAddress;\
			__set_MSP(*(__IO uint32_t*) ApplicationAddress);//初始化用户程序的堆栈指针
			Jump_To_Application();
	}
	
	printf("Bootloader error!\r\n");
}

/*
	串口重定向
	用于实现printf打印
*/
int fputc(int ch, FILE *f)
{
    HAL_UART_Transmit(&huart2, (uint8_t *)&ch,1, 0xFFFF);
    return ch;
}
