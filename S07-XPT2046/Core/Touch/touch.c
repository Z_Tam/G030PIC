/**********************
	触摸芯片：XPT2046
**********************/

#include "touch.h"
#include "stm32g0xx_hal.h"
#include "stdio.h"
#include "stdlib.h"


static uint8_t CMD[2] = {0xD0, 0x90};	/* X和Y坐标读取指令 */

#define timeout 60000
void touch_irq(void)
{
		for(uint16_t i=0;i<timeout;i++);
		touch_raw_to_coord();
}

/********************
	LL底层spi收发函数
*********************/
uint16_t SPI2_ReadWriteByte(uint8_t TxData)
{  
    uint8_t retry=0;     
    while((SPI2->SR&1<<1)==0)
    {
        retry++;
        if(retry>200)
				return 0;
    }     
    SPI2->DR =TxData; 
    retry=0;
    while((SPI2->SR&1<<0)==0)  
    {
        retry++;
        if(retry>200)
				return 0;
    }             
    return SPI2->DR;                
}

/*********************
	读取XY电压数据
**********************/
static void touch_read_raw(uint8_t *Vxy)
{
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_RESET);
	Vxy[0] = (SPI2_ReadWriteByte(CMD[0])>>8);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_SET);
	  
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_RESET);
	Vxy[1] = (SPI2_ReadWriteByte(CMD[1])>>8);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_SET);
}

/************************
	对读到的电压进行排序，
	取中间值
*************************/
#define READ_TIMES 5 	//读取次数
#define LOST_VAL 1		//丢弃值(去头去尾)
static void touch_sort_raw(uint16_t *Vx, uint16_t *Vy)
{
	uint8_t aver[READ_TIMES][2] = {0};
	uint8_t temp = 0;
	uint16_t Xsum = 0, Ysum = 0;
	
	/* 读出5组数据 */
	for(int i=0;i<READ_TIMES;i++)
	{	
		touch_read_raw(aver[i]);
	}
	
	/* 分别对x和y排序 */
	for(int k=0;k<2;k++)					
	{
		for(int i=0;i<READ_TIMES; i++)		/* 排序 */
		{
			for(int j=i+1;j<READ_TIMES;j++)
			{
				if(aver[i][k]>aver[j][k])	/* 升序排列 */
				{
					temp=aver[i][k];
					aver[i][k]=aver[j][k];
					aver[j][k]=temp;
				}
			}
		}	  
	}	
	
	/* 去头去尾，求和 */
	for(int j=LOST_VAL;j<READ_TIMES-LOST_VAL;j++)	
		Xsum+=aver[j][0];
	for(int j=LOST_VAL;j<READ_TIMES-LOST_VAL;j++)	
		Ysum+=aver[j][1];
	
	*Vx = Xsum/(READ_TIMES-2*LOST_VAL);
	*Vy = Ysum/(READ_TIMES-2*LOST_VAL);
}

/***********************
	检测是不是干扰，
	并给出最终电压值
***********************/
#define ERR_RANGE 50 //误差范围 
static uint8_t touch_check_raw(uint16_t *Vx, uint16_t *Vy)
{
	uint16_t x1,y1;
 	uint16_t x2,y2;
	
	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_5) == 0)
	{
		touch_sort_raw(&x1, &y1);
		touch_sort_raw(&x2, &y2);
		
		if( abs(x1-x2)<ERR_RANGE && abs(y1-y2)<ERR_RANGE )
		{
			*Vx=(x1+x2)/2;
			*Vy=(y1+y2)/2;
			return 1;
		}else 
			return 0;	  	
	}
	return 0;
}	

/*************************
	将xy电压转为坐标
	行：6 - 124
	列：7 - 127
**************************/
void touch_raw_to_coord(void)
{
	uint16_t x,y;
	if(touch_check_raw(&x, &y))
	{
		x = 1.084746*x + -6;
		y = 1.333333*y + -7;
		printf("X: %d, Y: %d\r\n",x,y);
	}
	
}




