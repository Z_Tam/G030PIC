/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd_cfg.h"
#include "lcd.h"
#include "arm_2d.h"
#include "arm_2d_helper.h"
#include "dog.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#define _Dog_Width 40
#define _Dog_Height 40

#define LED1 0x68
#define LED2 0x6A
#define LED3 0x6C
#define LED4 0x6E
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
//__attribute__((aligned(2)))
RTC_TimeTypeDef stimestructure;
RTC_DateTypeDef sdatestructure;
extern uint16_t gImage_dog[];
uint8_t table[] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x77,0x7c,0x39,0x5e,0x79,0x71};
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static arm_2d_helper_pfb_t s_tPFBHelper;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

uint8_t I2C_Write(I2C_TypeDef *I2Cx, unsigned char slave_addr, unsigned char* reg_data, unsigned char data_size,uint32_t timeout)
{
	uint32_t tickstart;
	unsigned char tx_num = 0;	
	tickstart = HAL_GetTick();
	
	while(LL_I2C_IsActiveFlag_BUSY(I2Cx) == SET)
	{
		if(HAL_GetTick() - tickstart > timeout)
			return 0;
	}
	LL_I2C_HandleTransfer(I2Cx, slave_addr, LL_I2C_ADDRSLAVE_7BIT, data_size, LL_I2C_MODE_AUTOEND, LL_I2C_GENERATE_START_WRITE);
	
	while(1)
	{
		if(LL_I2C_IsActiveFlag_TXE(I2Cx) && tx_num<data_size)
		{
			while(LL_I2C_IsActiveFlag_TXIS(I2Cx) == RESET);
			LL_I2C_TransmitData8(I2Cx, reg_data[tx_num++]);
				
		}else if(tx_num == data_size)
		{
			break;
		}
	}
	
	tickstart = HAL_GetTick();
	while(LL_I2C_IsActiveFlag_STOP(I2Cx) == RESET)
	{
		if(HAL_GetTick() - tickstart > timeout)
			return 0;
	}
	return 1;
}

void TIM1_IRQ(void)
{
	if(LL_TIM_IsActiveFlag_UPDATE(TIM1) == SET)
	{
		LL_TIM_ClearFlag_UPDATE(TIM1);
		LL_GPIO_TogglePin(GPIOC, LL_GPIO_PIN_13);
		HAL_RTC_GetTime(&hrtc, &stimestructure, RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc, &sdatestructure, RTC_FORMAT_BIN);
	
		I2C_Write(I2C1, LED1, &table[stimestructure.Minutes/10], 1, 0xff);  
		I2C_Write(I2C1, LED2, &table[stimestructure.Minutes%10], 1, 0xff);
		I2C_Write(I2C1, LED3, &table[stimestructure.Seconds/10], 1, 0xff);
		I2C_Write(I2C1, LED4, &table[stimestructure.Seconds%10], 1, 0xff);
	}
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*
** 配置图片源和图片大小
*/
const arm_2d_tile_t c_tileDoge = {
    .tRegion = {
        .tSize = {
            .iWidth = _Dog_Width,  //!< 素材的宽度
            .iHeight = _Dog_Height  //!< 素材d额高度
        },
    },
    .tInfo.bIsRoot = true,  //!< 说明这个贴图拥有资源
    .phwBuffer = (uint16_t *)gImage_dog,  //!< 指向资源的指针
  };


/*
** 链接硬件层函数  
*/
int32_t GLCD_DrawBitmap (uint32_t x, 
                         uint32_t y, 
                         uint32_t width, 
                         uint32_t height, 
                         const uint16_t *bitmap) 
{
	LCD_Fill(x, y,x+width,y+height,*bitmap);		/* 要填充的颜色 */
	return 0;
}

/* 使用pfb */
static void __pfb_render_handler( void *pTarget, const arm_2d_pfb_t *ptPFB)
{
    const arm_2d_tile_t *ptTile = &(ptPFB->tTile);

    ARM_2D_UNUSED(pTarget);

    GLCD_DrawBitmap(ptTile->tRegion.tLocation.iX,
                    ptTile->tRegion.tLocation.iY,
                    ptTile->tRegion.tSize.iWidth,
                    ptTile->tRegion.tSize.iHeight,
                    ptTile->pchBuffer);

    arm_2d_helper_pfb_report_rendering_complete(&s_tPFBHelper, (arm_2d_pfb_t *)ptPFB);
}


static arm_fsm_rt_t __pfb_draw_background_handler( void *pTarget,
                                          const arm_2d_tile_t *ptTile)
{
    ARM_2D_UNUSED(pTarget);
    
//    arm_2d_rgb16_fill_colour(
//        ptTile,      //!< 目标缓冲区
//        NULL,        //!< 填充目标缓冲区的哪个区域
//        WHITE); //!< 白色
	
	arm_2d_region_t tDogRegion = {
        .tLocation = {
            .iX = (LCD_W_IN - c_tileDoge.tRegion.tSize.iWidth) >> 1,
            .iY = (LCD_H_IN - c_tileDoge.tRegion.tSize.iHeight * 2) >> 1,
        },
        .tSize = c_tileDoge.tRegion.tSize,
    };
  
	/* 填充图片 */
    arm_2d_rgb16_tile_copy(
        &c_tileDoge,  //!< 我们的素材
        ptTile,       //!< 目标缓冲区
        NULL,         //!< 拷贝到目标缓冲区的那个区域
        ARM_2D_CP_MODE_COPY
	  | ARM_2D_CP_MODE_FILL); //!< 就是单纯的拷贝，不做作

	/* 添加蒙板 */
	arm_2d_rgb565_fill_colour_with_alpha(
        ptTile,       //!< 目标缓冲区
        NULL,         //!< 填充目标缓冲区的哪个区域
        //!< 特别指明是 rgb565 的白色
        (arm_2d_color_rgb565_t){WHITE}, 
        200);         //!< 不透明度 (200/255) * 100
	
	/* 指定位置显示图片 */
	arm_2d_rgb16_tile_copy(
        &c_tileDoge,      //!< 我们的素材
        ptTile,           //!< 目标缓冲区
        &tDogRegion,      // 拷贝到指定的区域
        ARM_2D_CP_MODE_COPY);
	
	/* 重新指定位置 */	
	tDogRegion.tLocation.iY += c_tileDoge.tRegion.tSize.iHeight;
		
	/* 图片Y镜像 */
	arm_2d_rgb16_tile_copy(
        &c_tileDoge,      //!< 我们的素材
        ptTile,           //!< 目标缓冲区
        &tDogRegion,      // 拷贝到指定的区域
        ARM_2D_CP_MODE_Y_MIRROR);
	/* Y镜像后添加蒙板 */	
	arm_2d_rgb565_fill_colour_with_alpha(
        ptTile,       //!< 目标缓冲区
        &tDogRegion,         //!< 填充目标缓冲区的哪个区域
        //!< 特别指明是 rgb565 的白色
        (arm_2d_color_rgb565_t){WHITE}, 
        100);         //!< 不透明度 (200/255) * 100
}		
		
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint8_t led = 0;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI2_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_RTC_Init();
  /* USER CODE BEGIN 2 */
  LCD_Init();
  
  I2C_Write(I2C1, 0x48, &led, 1, 0xff);
  led = 1;
  I2C_Write(I2C1, 0x48, &led, 1, 0xff);
  
  LL_TIM_EnableIT_UPDATE(TIM1);
  LL_TIM_EnableCounter(TIM1);
  
  I2C_Write(I2C1, LED1, &table[1], 1, 0xff);  
  I2C_Write(I2C1, LED2, &table[2], 1, 0xff);
  I2C_Write(I2C1, LED3, &table[3], 1, 0xff);
  I2C_Write(I2C1, LED4, &table[4], 1, 0xff);
  
  //LCD_Fill(0,0,LCD_W,LCD_H,WHITE);
 
	arm_2d_init();
	
  
	
//	//! initialise FPB helper
    if (ARM_2D_HELPER_PFB_INIT( 
            &s_tPFBHelper,     //!< FPB Helper object
            LCD_W_IN,               //!< screen width
            LCD_H_IN,               //!< screen height
            uint8_t,          //!< colour date type
            1,                //!< PFB block width
            1,                //!< PFB block height
            3,                 //!< number of PFB in the PFB pool
            {
                .evtOnLowLevelRendering = {
                    //! callback for low level rendering 
                    .fnHandler = &__pfb_render_handler,                         
                },
                .evtOnDrawing = {
                    //! callback for drawing GUI 
                    .fnHandler = &__pfb_draw_background_handler, 
                },
            }
        ) < 0) {
        //! error detected
        assert(false);
    }
		

		
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  while( arm_fsm_rt_cpl != arm_2d_helper_pfb_task(&s_tPFBHelper, NULL) );
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
  while(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2)
  {
  }

  /* HSE configuration and activation */
  LL_RCC_HSE_Enable();
  while(LL_RCC_HSE_IsReady() != 1)
  {
  }

  LL_PWR_EnableBkUpAccess();
  /* LSE configuration and activation */
  LL_RCC_LSE_SetDriveCapability(LL_RCC_LSEDRIVE_LOW);
  LL_RCC_LSE_Enable();
  while(LL_RCC_LSE_IsReady() != 1)
  {
  }

  /* Main PLL configuration and activation */
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_1, 16, LL_RCC_PLLR_DIV_2);
  LL_RCC_PLL_Enable();
  LL_RCC_PLL_EnableDomain_SYS();
  while(LL_RCC_PLL_IsReady() != 1)
  {
  }

  /* Set AHB prescaler*/
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);

  /* Sysclk activation on the main PLL */
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
  {
  }

  /* Set APB1 prescaler*/
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  /* Update CMSIS variable (which can be updated also through SystemCoreClockUpdate function) */
  LL_SetSystemCoreClock(64000000);

   /* Update the time base */
  if (HAL_InitTick (TICK_INT_PRIORITY) != HAL_OK)
  {
    Error_Handler();
  }
  if(LL_RCC_GetRTCClockSource() != LL_RCC_RTC_CLKSOURCE_LSE)
  {
    LL_RCC_ForceBackupDomainReset();
    LL_RCC_ReleaseBackupDomainReset();
    LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSE);
  }
  LL_RCC_EnableRTC();
  LL_RCC_SetI2CClockSource(LL_RCC_I2C1_CLKSOURCE_PCLK1);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
