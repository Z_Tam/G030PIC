#ifndef __EXTI_H
#define __EXTI_H

typedef struct
{
	void (* keybutton)(void);
	void (* usbcharge)(void);
	void (* lcdtouch)(void);
}EXTI_CALLBACK;
extern EXTI_CALLBACK _EXTI;


#endif
