/*************************
PB5: 屏幕触摸
PB0: 充电中
PB1: 充电完成
PA8: 按键
**************************/

#include "exti.h"
#include "stdio.h"
#include "gpio.h"

static void keybutton_callback(void);
static void usbcharge_callback(void);
static void lcdtouch_callback(void);

EXTI_CALLBACK _EXTI = 
{
	.keybutton = keybutton_callback,
	.usbcharge = usbcharge_callback,
	.lcdtouch = lcdtouch_callback,
};

#define WAIT 50000

/*********************************
		按键按下接3.3v
		需内部下拉
**********************************/
static void keybutton_callback(void)
{
	if (LL_EXTI_IsActiveRisingFlag_0_31(LL_EXTI_LINE_8) != RESET)			/* 上升沿 */
  {		
		for(uint16_t i=0;i<WAIT;i++);
		if(1 == LL_GPIO_IsInputPinSet(GPIOA,LL_GPIO_PIN_8))
			printf("key down\r\n");
		for(uint16_t i=0;i<WAIT;i++);
	}
	if (LL_EXTI_IsActiveFallingFlag_0_31(LL_EXTI_LINE_8) != RESET)		/* 下降沿 */
  {
		for(uint16_t i=0;i<WAIT;i++);
		if(0 == LL_GPIO_IsInputPinSet(GPIOA,LL_GPIO_PIN_8))
			printf("key up\r\n");
		for(uint16_t i=0;i<WAIT;i++);
	}
	
}

/********************************* 
		充电芯片IO为高阻态，
		需要内部拉高
**********************************/
static void usbcharge_callback(void)
{	
	static uint8_t PB0 = 0, PB1 = 0;
	
	for(uint16_t i=0;i<WAIT;i++);
	PB0 = LL_GPIO_IsInputPinSet(GPIOB,LL_GPIO_PIN_0);
	PB1 = LL_GPIO_IsInputPinSet(GPIOB,LL_GPIO_PIN_1);
		
	switch(PB0|PB1<<1)
	{
		case 1:
			printf("full!\r\n");
			break;
		case 2:
			printf("charge in!\r\n");
			break;
		case 3:
			printf("charge out!\r\n");
			break;
		default:
			break;
	}
	for(uint16_t i=0;i<WAIT;i++);

}

/**********************************
		屏幕接了上拉电阻，
		中断也配置为上拉
***********************************/
static void lcdtouch_callback(void)
{
	if (LL_EXTI_IsActiveRisingFlag_0_31(LL_EXTI_LINE_5) != RESET)			/* 上升沿 */
  {
		for(uint16_t i=0;i<WAIT;i++);
		if(1 == LL_GPIO_IsInputPinSet(GPIOB,LL_GPIO_PIN_5))
			printf("touch up\r\n");
		for(uint16_t i=0;i<WAIT;i++);
	}
	
	if (LL_EXTI_IsActiveFallingFlag_0_31(LL_EXTI_LINE_5) != RESET)		/* 下降沿 */
  {
		for(uint16_t i=0;i<WAIT;i++);
		if(0 == LL_GPIO_IsInputPinSet(GPIOB,LL_GPIO_PIN_5))
			printf("touch down\r\n");
		for(uint16_t i=0;i<WAIT;i++);
	}
	
}













