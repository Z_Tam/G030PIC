## 说明
- 交互软件：Xshell (不建议用XCOM)
- 使用参考：[FinSH控制台](https://www.rt-thread.org/document/site/programming-manual/finsh/finsh/)
- 移植参考：[移植控制台/FinSH](https://www.rt-thread.org/document/site/tutorial/nano/finsh-port/an0045-finsh-port/)

## RT-Thread shell commands:
|commands||
|:--|:--|
|version        | - show RT-Thread version information
|list_thread    | - list thread
|list_sem       | - list semaphore in system
|list_event     | - list event in system
|list_mutex     | - list mutex in system
|list_mailbox   | - list mail box in system
|list_msgqueue  | - list message queue in system
|list_timer     | - list timer in system
|list_device    | - list device in system
|exit           | - return to RT-Thread shell mode.
|help           | - RT-Thread shell help.
|ps             | - List threads in the system.
|time           | - Execute command with time.
|free           | - Show the memory usage in the system.

