- 所需环境： Python-3.8.6-amd64

- 更新pip命令：
pip install --upgrade pip

- 所需安装包：<br/>
  
|命令|包名   |版本   |
|:----|:----|:----|
|   初始|
|   |pip|19.0.3|
|   |setuptools|40.8.0|
|   pip3 install pyqtgraph|
|   |numpy |1.19.4|
|   |pyqtgraph|0.11.0|
|pip3 install PyQt5|
|   |PyQt5|5.15.1|
|   |PyQt5-sip|12.8.1|
|pip3 install pyinstaller|
|   |altgraph|0.17|
|   |future|0.18.2|
|   |pefile|2019.4.18|
|   |pyinstaller|4.0|
|   |pyinstaller-hooks-contrib|2020.10|
|   |pywin32-ctypes|0.2.0|
|pip3 install serial|
|   |serial|0.097|
|   |iso8601|0.1.13|
|   |PyYAML|5.3.1|
|pip3 install modbus_tk|
|   |modbus-tk|1.1.2|
|   |pyserial|3.4|




- tips:下载加速可在后面加上此地址: ```-i https://pypi.tuna.tsinghua.edu.cn/simple```
##

##
打包工具：<br/>
&emsp;pyinstaller<br/>
指令：<br/>
&emsp;```pyinstaller -w -F 文件名.py```<br/>
&emsp;(如果有多个文件，直接在后面加)
##

