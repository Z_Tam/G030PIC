/*********************************************************************
* PB13  SCL		SPI_CLK
* PB15  SDA		SPI_MOSI
* PA3   RES
* PA4   DC
* PA5   CS
* PA7   BLK
*********************************************************************/

// <<< Use Configuration Wizard in Context Menu >>>

// <h>屏幕尺寸
// <o>屏幕长
//  <i> LCD_H
//  <0-65536>
#define LCD_H_IN 160

// <o>屏幕宽
//  <i> LCD_W
//  <0-65536>
#define LCD_W_IN 128
// </h>

// <o>屏幕方向选择：
//  <i>  USE_HORIZONTAL：上
//  <0=> 上
//  <1=> 下
//  <3=> 左
//  <2=> 右
#define USE_HORIZONTAL 0

// <<< end of configuration section >>>


/* 
** 根据屏幕方向自动切换长宽
** 这里的 “LCD_W” 和 “LCD_H” 为当前屏幕方向的 “宽” 和 “高”
*/
#if (USE_HORIZONTAL == 0 || USE_HORIZONTAL == 1)
	#define LCD_W LCD_W_IN
	#define LCD_H LCD_H_IN
#elif (USE_HORIZONTAL == 2 || USE_HORIZONTAL == 3)
	#define LCD_W LCD_H_IN
	#define LCD_H LCD_W_IN
#endif


#ifndef _LCD_CFG_H
#define _LCD_CFG_H

#include "stm32g0xx_hal.h"

#define delay_ms(x) HAL_Delay(x)
#define LCD_RES(x) 	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, x)
#define LCD_DC(x)	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, x)
#define LCD_CS(x)	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, x)
#define LCD_BLK(x)	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, x)

#endif
