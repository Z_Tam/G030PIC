#include "lcd.h"
#include "lcd_cfg.h"
#include "pic.h"


/********************************************************************
* 函数说明：在指定区域填充颜色
*********************************************************************/
void LCD_Fill(uint16_t xsta,		/* 起始坐标 */
			  uint16_t ysta,
              uint16_t xend,		/* 终止坐标 */
			  uint16_t yend,
			  uint16_t color)		/* 要填充的颜色 */
{          
	uint16_t i,j; 
	LCD_Address_Set(xsta,ysta,xend-1,yend-1);//设置显示范围

	for(i=ysta;i<yend;i++)
	{													   	 	
		for(j=xsta;j<xend;j++)
		{
			LCD_WR_DATA16(color);
		}
	} 			
}


//点，线，圆，矩形

/********************************************************************
* 函数说明：在指定位置画点
* 入口数据：x,y 画点坐标
*           color 点的颜色
*********************************************************************/
void LCD_DrawPoint(uint16_t x,uint16_t y,uint16_t color)
{
	LCD_Address_Set(x,y,x,y);//设置光标位置 
	LCD_WR_DATA16(color);
} 

/********************************************************************
* 函数说明：画线
********************************************************************/
void LCD_DrawLine(uint16_t x1,		/* 起始坐标*/
				  uint16_t y1,
				  uint16_t x2,		/* 终止坐标*/
				  uint16_t y2,
				  uint16_t color)	/* 线的颜色*/
{
	uint16_t t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance;
	int incx,incy,uRow,uCol;
	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1;
	uRow=x1;//画线起点坐标
	uCol=y1;
	if(delta_x>0)incx=1; //设置单步方向 
	else if (delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;}
	if(delta_y>0)incy=1;
	else if (delta_y==0)incy=0;//水平线 
	else {incy=-1;delta_y=-delta_y;}
	if(delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y;
	for(t=0;t<distance+1;t++)
	{
		LCD_DrawPoint(uRow,uCol,color);//画点
		xerr+=delta_x;
		yerr+=delta_y;
		if(xerr>distance)
		{
			xerr-=distance;
			uRow+=incx;
		}
		if(yerr>distance)
		{
			yerr-=distance;
			uCol+=incy;
		}
	}
}

/********************************************************************
* 函数说明：画矩形
********************************************************************/
void LCD_DrawRectangle(uint16_t x1, 		/* 起始坐标 */
					   uint16_t y1, 
					   uint16_t x2, 		/* 终止坐标 */
					   uint16_t y2,
					   uint16_t color)		/* 矩形的颜色 */
{
	LCD_DrawLine(x1,y1,x2,y1,color);
	LCD_DrawLine(x1,y1,x1,y2,color);
	LCD_DrawLine(x1,y2,x2,y2,color);
	LCD_DrawLine(x2,y1,x2,y2,color);
}

/********************************************************************
* 函数说明：画圆
********************************************************************/
void Draw_Circle(uint16_t x0,		/* 圆心坐标 */
				 uint16_t y0,		
				 uint8_t r,			/* 半径 */
				 uint16_t color)	/* 圆的颜色 */
{
	int a,b;
	a=0;b=r;	  
	while(a<=b)
	{
		LCD_DrawPoint(x0-b,y0-a,color);             //3           
		LCD_DrawPoint(x0+b,y0-a,color);             //0           
		LCD_DrawPoint(x0-a,y0+b,color);             //1                
		LCD_DrawPoint(x0-a,y0-b,color);             //2             
		LCD_DrawPoint(x0+b,y0+a,color);             //4               
		LCD_DrawPoint(x0+a,y0-b,color);             //5
		LCD_DrawPoint(x0+a,y0+b,color);             //6 
		LCD_DrawPoint(x0-b,y0+a,color);             //7
		a++;
		if((a*a+b*b)>(r*r))//判断要画的点是否过远
		{
			b--;
		}
	}
}


//英文

/********************************************************************
* 函数说明：显示单个字符（英文）
********************************************************************/
void LCD_ShowChar(uint16_t x,		/* 显示坐标 */
				  uint16_t y,
				  uint8_t num,		/* 要显示的字符 */
				  uint16_t fc,		/* 字的颜色 */
				  uint16_t bc,		/* 字的背景色 */
				  uint8_t sizey,	/* 字号 */
				  uint8_t mode)		/* 0非叠加模式  1叠加模式 */
{
	uint8_t temp,sizex,t,m=0;
	uint16_t i,TypefaceNum;//一个字符所占字节大小
	uint16_t x0=x;
	sizex=sizey/2;
	TypefaceNum=(sizex/8+((sizex%8)?1:0))*sizey;
	num=num-' ';    //得到偏移后的值
	LCD_Address_Set(x,y,x+sizex-1,y+sizey-1);  		//设置光标位置 
	for(i=0;i<TypefaceNum;i++)
	{ 
		if(sizey==12)temp=ascii_1206[num][i];		//调用6x12字体
		else if(sizey==16)temp=ascii_1608[num][i];	//调用8x16字体
		else if(sizey==24)temp=ascii_2412[num][i];	//调用12x24字体
		else if(sizey==32)temp=ascii_3216[num][i];	//调用16x32字体
		else return;
		for(t=0;t<8;t++)
		{
			if(!mode)//非叠加模式
			{
				if(temp&(0x01<<t))LCD_WR_DATA16(fc);
				else LCD_WR_DATA16(bc);
				m++;
				if(m%sizex==0)
				{
					m=0;
					break;
				}
			}
			else//叠加模式
			{
				if(temp&(0x01<<t))LCD_DrawPoint(x,y,fc);//画一个点
				x++;
				if((x-x0)==sizex)
				{
					x=x0;
					y++;
					break;
				}
			}
		}
	}   	 	  
}

/********************************************************************
* 函数说明：显示字符串（英文）
********************************************************************/
void LCD_ShowString(uint16_t x,			/* 显示坐标 */
					uint16_t y,
					const uint8_t *p,	/* 要显示的字符串 */
					uint16_t fc,		/* 字的颜色 */
					uint16_t bc,		/* 字的背景色 */
					uint8_t sizey,		/* 字号 */
					uint8_t mode)		/* 0非叠加模式  1叠加模式 */
{         
	while(*p!='\0')
	{       
		LCD_ShowChar(x,y,*p,fc,bc,sizey,mode);
		x+=sizey/2;
		p++;
	}  
}


//中文

/********************************************************************
* 函数说明：显示单个12x12汉字
********************************************************************/
static void LCD_ShowChinese12x12(uint16_t x,		/* 显示坐标 */
								 uint16_t y,
								 uint8_t *s,		/* 要显示的汉字 */
								 uint16_t fc,		/* 字的颜色 */
								 uint16_t bc,		/* 字的背景色 */
								 uint8_t sizey,		/* 字号 */
								 uint8_t mode)		/* 0非叠加模式  1叠加模式 */
{
	uint8_t i,j,m=0;
	uint16_t k;
	uint16_t HZnum;//汉字数目
	uint16_t TypefaceNum;//一个字符所占字节大小
	uint16_t x0=x;
	TypefaceNum=(sizey/8+((sizey%8)?1:0))*sizey;
	                         
	HZnum=sizeof(tfont12)/sizeof(typFNT_GB12);	//统计汉字数目
	for(k=0;k<HZnum;k++) 
	{
		if((tfont12[k].Index[0]==*(s))&&(tfont12[k].Index[1]==*(s+1)))
		{ 	
			LCD_Address_Set(x,y,x+sizey-1,y+sizey-1);
			for(i=0;i<TypefaceNum;i++)
			{
				for(j=0;j<8;j++)
				{	
					if(!mode)//非叠加方式
					{
						if(tfont12[k].Msk[i]&(0x01<<j))LCD_WR_DATA16(fc);
						else LCD_WR_DATA16(bc);
						m++;
						if(m%sizey==0)
						{
							m=0;
							break;
						}
					}
					else//叠加方式
					{
						//画一个点
						if(tfont12[k].Msk[i]&(0x01<<j))	LCD_DrawPoint(x,y,fc);
						x++;
						if((x-x0)==sizey)
						{
							x=x0;
							y++;
							break;
						}
					}
				}
			}
		}				  	
		continue;  //查找到对应点阵字库立即退出，防止多个汉字重复取模带来影响
	}
} 

/********************************************************************
* 函数说明：显示单个16x16汉字
********************************************************************/
static void LCD_ShowChinese16x16(uint16_t x,		/* 显示坐标 */
								 uint16_t y,	
								 uint8_t *s,		/* 要显示的汉字 */
								 uint16_t fc,		/* 字的颜色 */
								 uint16_t bc,		/* 字的背景色 */
								 uint8_t sizey,		/* 字号 */
								 uint8_t mode)		/* 0非叠加模式  1叠加模式 */
{
	uint8_t i,j,m=0;
	uint16_t k;
	uint16_t HZnum;//汉字数目
	uint16_t TypefaceNum;//一个字符所占字节大小
	uint16_t x0=x;
  TypefaceNum=(sizey/8+((sizey%8)?1:0))*sizey;
	HZnum=sizeof(tfont16)/sizeof(typFNT_GB16);	//统计汉字数目
	for(k=0;k<HZnum;k++) 
	{
		if ((tfont16[k].Index[0]==*(s))&&(tfont16[k].Index[1]==*(s+1)))
		{ 	
			LCD_Address_Set(x,y,x+sizey-1,y+sizey-1);
			for(i=0;i<TypefaceNum;i++)
			{
				for(j=0;j<8;j++)
				{	
					if(!mode)//非叠加方式
					{
						if(tfont16[k].Msk[i]&(0x01<<j))LCD_WR_DATA16(fc);
						else LCD_WR_DATA16(bc);
						m++;
						if(m%sizey==0)
						{
							m=0;
							break;
						}
					}
					else//叠加方式
					{
						if(tfont16[k].Msk[i]&(0x01<<j))	LCD_DrawPoint(x,y,fc);//画一个点
						x++;
						if((x-x0)==sizey)
						{
							x=x0;
							y++;
							break;
						}
					}
				}
			}
		}				  	
		continue;  //查找到对应点阵字库立即退出，防止多个汉字重复取模带来影响
	}
} 

/********************************************************************
* 函数说明：显示单个24x24汉字
********************************************************************/
static void LCD_ShowChinese24x24(uint16_t x,		/* 显示坐标 */
								 uint16_t y,
								 uint8_t *s,		/* 要显示的汉字 */
								 uint16_t fc,		/* 字的颜色 */
								 uint16_t bc,		/* 字的背景色 */
								 uint8_t sizey,		/* 字号 */
								 uint8_t mode)		/* 0非叠加模式  1叠加模式 */
{
	uint8_t i,j,m=0;
	uint16_t k;
	uint16_t HZnum;//汉字数目
	uint16_t TypefaceNum;//一个字符所占字节大小
	uint16_t x0=x;
	TypefaceNum=(sizey/8+((sizey%8)?1:0))*sizey;
	HZnum=sizeof(tfont24)/sizeof(typFNT_GB24);	//统计汉字数目
	for(k=0;k<HZnum;k++) 
	{
		if ((tfont24[k].Index[0]==*(s))&&(tfont24[k].Index[1]==*(s+1)))
		{ 	
			LCD_Address_Set(x,y,x+sizey-1,y+sizey-1);
			for(i=0;i<TypefaceNum;i++)
			{
				for(j=0;j<8;j++)
				{	
					if(!mode)//非叠加方式
					{
						if(tfont24[k].Msk[i]&(0x01<<j))LCD_WR_DATA16(fc);
						else LCD_WR_DATA16(bc);
						m++;
						if(m%sizey==0)
						{
							m=0;
							break;
						}
					}
					else//叠加方式
					{
						if(tfont24[k].Msk[i]&(0x01<<j))	LCD_DrawPoint(x,y,fc);//画一个点
						x++;
						if((x-x0)==sizey)
						{
							x=x0;
							y++;
							break;
						}
					}
				}
			}
		}				  	
		continue;  //查找到对应点阵字库立即退出，防止多个汉字重复取模带来影响
	}
} 

/********************************************************************
* 函数说明：显示单个32x32汉字
********************************************************************/
static void LCD_ShowChinese32x32(uint16_t x,		/* 显示坐标 */
								 uint16_t y,
								 uint8_t *s,		/* 要显示的汉字 */
								 uint16_t fc,		/* 字的颜色 */
								 uint16_t bc,		/* 字的背景色 */
								 uint8_t sizey,		/* 字号 */
								 uint8_t mode)		/* 0非叠加模式  1叠加模式 */
{
	uint8_t i,j,m=0;
	uint16_t k;
	uint16_t HZnum;//汉字数目
	uint16_t TypefaceNum;//一个字符所占字节大小
	uint16_t x0=x;
	TypefaceNum=(sizey/8+((sizey%8)?1:0))*sizey;
	HZnum=sizeof(tfont32)/sizeof(typFNT_GB32);	//统计汉字数目
	for(k=0;k<HZnum;k++) 
	{
		if ((tfont32[k].Index[0]==*(s))&&(tfont32[k].Index[1]==*(s+1)))
		{ 	
			LCD_Address_Set(x,y,x+sizey-1,y+sizey-1);
			for(i=0;i<TypefaceNum;i++)
			{
				for(j=0;j<8;j++)
				{	
					if(!mode)//非叠加方式
					{
						if(tfont32[k].Msk[i]&(0x01<<j))LCD_WR_DATA16(fc);
						else LCD_WR_DATA16(bc);
						m++;
						if(m%sizey==0)
						{
							m=0;
							break;
						}
					}
					else//叠加方式
					{
						//画一个点
						if(tfont32[k].Msk[i]&(0x01<<j))	LCD_DrawPoint(x,y,fc);
						x++;
						if((x-x0)==sizey)
						{
							x=x0;
							y++;
							break;
						}
					}
				}
			}
		}				  	
		continue;  //查找到对应点阵字库立即退出，防止多个汉字重复取模带来影响
	}
}

/********************************************************************
* 函数说明：显示汉字串
********************************************************************/
void LCD_ShowChinese(uint16_t x,		/* 显示坐标 */
					 uint16_t y,
					 uint8_t *s,		/* 要显示的汉字串 */
					 uint16_t fc,		/* 字的颜色 */
					 uint16_t bc,		/* 字的背景色 */
					 uint8_t sizey,		/* 字号 可选 12 16 24 32 */
					 uint8_t mode)		/* 0非叠加模式  1叠加模式 */
{
	while(*s!=0)
	{
		if(sizey==12) LCD_ShowChinese12x12(x,y,s,fc,bc,sizey,mode);
		else if(sizey==16) LCD_ShowChinese16x16(x,y,s,fc,bc,sizey,mode);
		else if(sizey==24) LCD_ShowChinese24x24(x,y,s,fc,bc,sizey,mode);
		else if(sizey==32) LCD_ShowChinese32x32(x,y,s,fc,bc,sizey,mode);
		else return;
		s+=2;
		x+=sizey;
	}
}


//数字

