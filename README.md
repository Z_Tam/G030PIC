# STM32G030C8T6-Demos

STM32G030C8T6示例工程，Python小工具。
## 资料地址     
教程地址：[个人博客](http://z_tam.gitee.io/blog/#/%E4%BD%9C%E5%93%81%E9%9B%86/README)    
代码地址：[gitee](https://gitee.com/Z_Tam/G030PIC)  
购买链接：  

## 硬件靓照

<div align=center>
<img src="_img/G030.jpg" width = 50%/>
</div>


## 系列说明

**A系列**：算法例子

| 序号 | 工程名称 | 说明 |
|---|---|---|
| A01 | BootLoader | BootLoader跳转 |
| A01 | BootLoader_IAP | 串口Ymodom下载App |
| A02 | HW_CRC | 硬件CRC16、软件计算CRC16、CRC16查表 |
| A03 | LCD_Lucency| LCD显示透明色块 |
| A04 | LCD_Dog | 狗头平铺，居中显示，y对称，蒙板 |

</br>

**B系列**：基础工程

| 序号 | 工程名称 | 说明 | 未解决问题 |
|---|---|---| --- |
| B01 | LED | LED流水灯闪烁 |
| B02 | USART | 串口1中断实现数据自收自发 |  
| B02 | USART-FIFO | 串口1队列接收 | 缓存大于最大值会溢出 | 
| B03 | KEY | 按钮支持单击，连按，长按检测 |  
| B04 | RTC | 外部晶振实时时钟|
| B05 | TIMER | 500ms定时器中断 |
| B06 | LL_PWR ||
| B07 | LL_EXTI ||
| B08 | SPI_DMA | SCK:PB8 </br> MOSI:PB7 </br> MISO:PB6|

</br>

**P系列**：Python工具

| 序号 | 工程名称 | 说明 |
|---|---|---|
| P01 | Py_IAP1 | IAP串口下载程序(无界面) |
| P01 | Py_IAP2 | IAP串口下载程序(PyQt5界面) |

</br>

**R系列**：RT-Thread nano工程

| 序号 | 工程名称 | 说明 |
|---|---|---|
| R01 | LED | 单线程流水灯 |
| R02 | FinSH| 串口命令交互 |

</br>

**S系列**：传感器工程

| 序号 | 工程名称 | 说明 |
|---|---|---|
| S01 | OLED | 屏幕显示中文 |
| S02 | SHT30_ARP | 温湿度传感器 |
| S03 | SHT30_DIS | 温湿度传感器 |
| S04 | W25Q64 | 片外非易失性存储 |
| S05 | LCD | LCD彩屏驱动 |
| S06 | AIP650EO | 八段数码管显示 |
| S07 | XPT2046 | 屏幕触摸芯片驱动 |
| S08 | LCD_DMA | SPI DMA屏幕驱动 |

</br>

**C系列**：综合历程

| 序号 | 工程名称 | 说明 | 相关工程 |
|---|---|---|---|
| C01 | LCD_Clock | 八段数码管时钟 | A04、B04、S06 |

</br>


## 开发环境
MDK5

</br>

## 备忘录
### 待开发
- rtt线程管理
- rtt邮箱
- cJSON   
- modbus
- W25Q64文件系统/字库
- esp8266     





