#ifndef __LEAF_OTA_H_
#define __LEAF_OTA_H_
#include "main.h"
/*=====用户配置(根据自己的分区进行配置)=====*/
#define ApplicationAddress    0x08005000U			//APP地址(20k)

/*==========================================*/
typedef void (*Jump_Fun)(void);	//类型声明

/*=================函数声明=================*/
void show_msg(void);
void JumpToApplication(void);
/*==========================================*/

#endif

